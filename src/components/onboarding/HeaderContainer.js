import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image } from 'react-native'
import { screenWidth } from '../../global/constants'
import { media } from '../../global/media'
  

const HeaderContainer = ({children}) => {
    return (
        <View style={{width: screenWidth, backgroundColor: '#fff'}} >
            <View style={{height: 450, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FEA7A9' }} >
                {children}
            </View>
            <Image source={media.wave} style={{height: 100, width: '100%',}} />
        </View>
    )
}

export default HeaderContainer
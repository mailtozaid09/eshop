import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, } from 'react-native';
import { fontSize } from '../../global/fontFamily';
import { colors } from '../../global/colors';
import { media } from '../../global/media';
import { screenWidth } from '../../global/constants';

const HomeHeader = ({title, subTitle, navigation}) => {
    return (
        <View style={styles.container} >
            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('Profile')}
                    style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.dark_blue, marginRight: 12}} >
                        <Image source={media.user} style={{height: 22, width: 22, resizeMode: 'contain', marginLeft: 4}} />
                </TouchableOpacity>
                <View>
                        <Text style={styles.heading} >Welcome</Text>
                        <Text style={styles.title} >Zaid Ahmed</Text>
                </View>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}} >
                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('Search')}
                    style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.light_gray, marginLeft: 12}} >
                        <Image source={media.search} style={{height: 22, width: 22, resizeMode: 'contain', marginLeft: 4}} />
                </TouchableOpacity>
                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('Notifications')}
                    style={{height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.light_gray, marginLeft: 12}} >
                        <Image source={media.bell} style={{height: 22, width: 22, resizeMode: 'contain', marginLeft: 4}} />
                </TouchableOpacity> 
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: screenWidth,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        paddingBottom: 10
    },
    heading: {
        fontSize: 12,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    title: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
    },
  
})

export default HomeHeader
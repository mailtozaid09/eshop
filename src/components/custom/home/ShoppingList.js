import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, ScrollView, } from 'react-native';
import { colors } from '../../../global/colors';
import { screenWidth } from '../../../global/constants';
import { fontSize } from '../../../global/fontFamily';
import { media } from '../../../global/media';

import { useDispatch, useSelector } from 'react-redux';
import { addItemToCart, addItemToWishlist, removeItemFromCart, removeItemFromWishlist } from '../../../redux/home/HomeActions';
import { useNavigation } from '@react-navigation/native';



const ShoppingList = ({shoppingData, isHorizontal}) => {

    const dispatch = useDispatch();

    const navigation = useNavigation();

    const cart_list = useSelector(state => state.HomeReducer.cart_list);

    const wish_list = useSelector(state => state.HomeReducer.wish_list);


    const itemIncludedInCartList = (item_name) => {
        return cart_list?.some(value => value['item_name'] == item_name);
    }

   const addItemToCartFunc = (item) => {

    if(itemIncludedInCartList(item.item_name)){
        dispatch(removeItemFromCart(item.item_name))
    }else{
        var favItem = {
            image_url: item.image_url,
            category: item.category,
            item_name: item.item_name,
            item_description: item.item_description,
            price: item.price,
            ratings: item.ratings,
        }
        dispatch(addItemToCart(favItem))
    }
   
}




    const itemIncludedInWishlist = (item_name) => {
        return wish_list?.some(value => value['item_name'] == item_name);
    }

    const addItemToWishlistFunc = (item) => {

        if(itemIncludedInWishlist(item.item_name)){
            dispatch(removeItemFromWishlist(item.item_name))
        }else{
            var favItem = {
                image_url: item.image_url,
                category: item.category,
                item_name: item.item_name,
                item_description: item.item_description,
                price: item.price,
                ratings: item.ratings,
            }
            dispatch(addItemToWishlist(favItem))
        }
    }

        
    return (
        <View style={styles.container} >

                <ScrollView horizontal={isHorizontal ? true : false} >
                <View style={isHorizontal ? {flexDirection: 'row',} : {flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                    {shoppingData.map((item, index) => (
                        <TouchableOpacity 
                        activeOpacity={0.5}
                        onPress={() => {navigation.navigate('ShoppingItemDetails', {params: item})}}
                        key={item.id} style={{width: isHorizontal ? 220 : screenWidth/2-35, marginBottom: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: colors.white, padding: 10, borderWidth: 1, borderColor: colors.light_gray, margin: 3}} >
                        <Image source={{uri: item.image_url}} style={{height: 200, borderRadius: 10, width: '100%', resizeMode: 'contain'}} />
                        <View style={{width: '100%'}} >
                            <Text numberOfLines={2} style={styles.title} >{item.item_name}</Text>
                            <Text numberOfLines={3} style={styles.description} >{item.item_description}</Text>
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} >
                                    <Image source={media.star} style={{height: 12,width: 12, marginRight: 6}} />
                                    <Text style={styles.ratings} >{item.ratings}</Text>
                                </View>
                                <Text style={styles.price} >Rs. {item.price}</Text>
                            </View>

                            {itemIncludedInCartList(item.item_name) 
                                ? 
                                <TouchableOpacity 
                                    onPress={() => addItemToCartFunc(item)}
                                    style={{backgroundColor: colors.white, borderColor: colors.dark_blue, borderWidth: 1, marginTop: 12, height: 40, borderRadius: 6, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}} >

                                    <Text style={{fontSize: fontSize.SubTitle, color: colors.dark_blue, fontWeight: '600'}} >REMOVE</Text>
                                </TouchableOpacity>
                                    
                                :
                                <TouchableOpacity 
                                    onPress={() => addItemToCartFunc(item)}
                                    style={{backgroundColor: colors.dark_blue, marginTop: 12, height: 40, borderRadius: 6, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}} >
                                    <Image source={media.plus} style={{height: 24, width: 24, marginRight: 10}} />
                                    <Text style={{fontSize: fontSize.SubTitle, color: colors.white, fontWeight: '600'}} >ADD</Text>
                                </TouchableOpacity>
                            }
                        </View>


                        <TouchableOpacity 
                            activeOpacity={0.5}
                            onPress={() => addItemToWishlistFunc(item)}
                            style={{position: 'absolute', top: 12, right: 12, height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.light_gray}} >
                            <Image source={itemIncludedInWishlist(item.item_name)  ? media.heart_fill : media.heart_outline} style={{height: 24, width: 24}} />
                        </TouchableOpacity>
                    </TouchableOpacity>
                    ))}
                    </View>
                </ScrollView>
          
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    title: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 10,
    },
    description: {
        fontSize: fontSize.Body,
        color: colors.dark_blue,
        fontWeight: '700',
        marginBottom: 10
    },
    ratings: {
        fontSize: 16,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    price: {
        fontSize: 18,
        color: colors.dark_blue,
        fontWeight: '700',
    },
})

export default ShoppingList
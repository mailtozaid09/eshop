import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, } from 'react-native';
import { colors } from '../../../global/colors';
import { screenWidth } from '../../../global/constants';
import { fontSize } from '../../../global/fontFamily';
import { media } from '../../../global/media';
import { categoriesData } from '../../../global/sampleData';

const CategoriesList = ({changeCategoryFilter}) => {

    const [currentCategory, setCurrentCategory] = useState('All');
    
    return (
        <View style={styles.container} >

            <FlatList
                data={categoriesData}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item) => item.id}
                renderItem={({item}) => {
                    return(
                        <TouchableOpacity 
                            onPress={() => {
                                setCurrentCategory(item.category); 
                                changeCategoryFilter(item.category)}}
                            activeOpacity={0.5}
                            style={{height: 130, width: 80, borderRadius: 50, marginRight: 15,  alignItems:'center', justifyContent: 'space-between', paddingVertical: 20, backgroundColor: item.category == currentCategory ? colors.dark_blue : colors.light_gray }} >
                            <View style={{height: 50, width: 50, borderRadius: 25, backgroundColor: colors.white, alignItems:'center', justifyContent: 'center', }} >
                                <Image source={item.categoryIcon} style={{height: item.category == 'All' ? 24 : 50, width: item.category == 'All' ? 24 : 50, borderRadius: item.category == 'All' ? null : 25, resizeMode: 'contain'}} />
                            </View>
                            <Text style={[styles.title, {color: item.category == currentCategory ? colors.white : colors.dark_blue}]} >{item.category}</Text>
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //alignItems: 'center',
        paddingHorizontal: 20,
        height: 150,
    },
    title: {
        fontSize: 12,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 10,
        marginBottom: 10
    },
})

export default CategoriesList
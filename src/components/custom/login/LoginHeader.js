import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, } from 'react-native';
import { colors } from '../../../global/colors';
import { screenWidth } from '../../../global/constants';
import { fontSize } from '../../../global/fontFamily';
import { media } from '../../../global/media';

const LoginHeader = ({title, subTitle}) => {
    return (
        <View style={styles.container} >
            <Image source={media.app_logo_white} style={styles.mainImg} />
            <Text style={styles.title} >{title}</Text>
            <Text style={styles.subtitle} >{subTitle}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    title: {
        fontSize: fontSize.Heading,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    subtitle: { 
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '600',
        // margin: 20,
        marginBottom: 40,
        marginTop: 20,
        textAlign: 'center'
    },
    mainImg: {
        width: 200, 
        height: 200, 
        borderRadius: 20,
        margin: 30,
    },
})

export default LoginHeader
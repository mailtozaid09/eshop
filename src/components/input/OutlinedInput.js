import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';

const OutlinedInput = ({ placeholder, isSearch, inputIcon, label, value,onClearSearchText, onChangeText, onChangeEyeIcon, isPassword, showEyeIcon }) => {
    return (
        <View>

        {/* <Text style={styles.label} >{label}</Text> */}

        <View style={[styles.inputContainer]}  >

            <Image source={inputIcon} style={[styles.iconImage, {marginRight: 15}]} />

            <TextInput
                placeholder={placeholder}
                placeholderTextColor={colors.gray}
                style={styles.inputStyle}
                value={value}
                onChangeText={onChangeText}
                secureTextEntry={isPassword  && !showEyeIcon ? true : false}
            />  

            {isPassword && (
                <TouchableOpacity onPress={onChangeEyeIcon}>
                    <Image source={showEyeIcon ? media.view : media.hide} style={styles.iconImage} />
                </TouchableOpacity>
            )}

            {/* {isSearch && (
                <TouchableOpacity disabled={true} onPress={onChangeEyeIcon}>
                    <Image source={media.search} style={[styles.iconImage, {marginRight: 15}]} />
                </TouchableOpacity>
            )}

            <TextInput
                placeholder={placeholder}
                placeholderTextColor={colors.gray}
                style={styles.inputStyle}
                value={value}
                onChangeText={onChangeText}
                secureTextEntry={isPassword  && !showEyeIcon ? true : false}
            />

            

            {isSearch && value && (
                <TouchableOpacity onPress={onClearSearchText}>
                    <Image source={media.close} style={[styles.iconImage, {position: 'absolute', right: -5}]} />
                </TouchableOpacity>
            )} */}
        </View>
        </View>
    )
}


const styles = StyleSheet.create({
    inputContainer: {
        height: 50,
        padding: 10,
        //backgroundColor: colors.red,
        borderBottomWidth: 1,
        borderColor: colors.gray,
        borderRadius: 10,
        width: screenWidth-40,
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
        //justifyContent: 'space-between'
    },
    inputStyle: {
        fontSize: fontSize.SubTitle,
        fontWeight: '500',
        color: colors.black,
        flex: 1,
        height: 40,
        //backgroundColor: 'red'
    },
    iconImage: {
        height: 24,
        width: 24
    },
    label: {
        fontSize: fontSize.Body,
        color: colors.black,
        fontWeight: 'bold',
        marginBottom: 6,
    }
})

export default OutlinedInput
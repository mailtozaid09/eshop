import React, { useState } from 'react'
import {Text, View, Button, StyleSheet, ScrollView} from 'react-native'
import RBSheet from 'react-native-raw-bottom-sheet'
import Input from '../input'
import { fontSize } from '../../global/fontFamily'
import { colors } from '../../global/colors'
import LoginButton from '../button/LoginButton'
import { useDispatch, useSelector } from 'react-redux'
import { addShippingAddress } from '../../redux/home/HomeActions'

const ShippingAddressSheet = ({refRBSheet, closeRBSHEET}) => {

    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});

    const dispatch = useDispatch();

  
   
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const saveAddressFunc = () => {
        console.log("form => ", form);

        var favItem = {
            fullName: (form.fullName).toString(),
            number: (form.number).toString(),
            address: (form.address).toString(),
            city: (form.city).toString(),
            region: (form.region).toString(),
            zipcode: (form.zipcode).toString(),
            country: (form.country.toString()),
        }
        dispatch(addShippingAddress(favItem))
        closeRBSHEET() 
      
    }



    return (
        <View style={{}}>
           
           <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                height={600}
                openDuration={250}
                customStyles={{
                    container: {
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        padding: 20
                    },
                    wrapper: {
                        backgroundColor: "#00000080"
                    },
                    draggableIcon: {
                        backgroundColor: "#000",
                        // marginBottom: 150
                    }
                }}
                >
                        <View style={{ }} >
                            <ScrollView>
                                <Text style={styles.header} >Add Shipping Address</Text>
                                <View style={{marginBottom: 100}} >
                                <Input
                                    label="Full Name"
                                    placeholder="Full Name"
                                    onChangeText={(text) => onChange({name: 'fullName', value: text})}
                                />

                                <Input
                                    label="Mobile Number"
                                    placeholder="Mobile Number"
                                    onChangeText={(text) => onChange({name: 'number', value: text})}
                                />
                                
                                <Input
                                    label="Address"
                                    placeholder="Address"
                                    onChangeText={(text) => onChange({name: 'address', value: text})}
                                />

                                <Input
                                    label="City"
                                    placeholder="City"
                                    onChangeText={(text) => onChange({name: 'city', value: text})}
                                />

                                <Input
                                    label="State/Provision/Region"
                                    placeholder="State/Provision/Region"
                                    onChangeText={(text) => onChange({name: 'region', value: text})}
                                />

                                <Input
                                    label="Zipcode"
                                    placeholder="Zipcode"
                                    onChangeText={(text) => onChange({name: 'zipcode', value: text})}
                                />

                                <Input
                                    label="Country"
                                    placeholder="Country"
                                    onChangeText={(text) => onChange({name: 'country', value: text})}
                                />
                                </View>

                               
                            </ScrollView>

                            <View style={{position: 'absolute', bottom: 20}} >
                                <LoginButton
                                    title="SAVE ADDREESS"
                                    onPress={() => {saveAddressFunc()}}
                                />
                            </View>
                        </View>
            </RBSheet>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
        padding: 20,
    },
    header: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 10,
        textAlign: 'center'
    },
});


export default ShippingAddressSheet
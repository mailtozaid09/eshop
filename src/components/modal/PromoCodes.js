import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, Modal, Pressable, SafeAreaView, ScrollView, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';
import { promoCodesData } from '../../global/sampleData';
import { useDispatch, useSelector } from 'react-redux';
import { setPromoCodeValue } from '../../redux/home/HomeActions';


const PromoCodes = ({showPromoModal, closeModalFunc}) => {

    const dispatch = useDispatch();

    const promo_code = useSelector(state => state.HomeReducer.promo_code);

    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={showPromoModal}
                onRequestClose={closeModalFunc}>
                <View style={styles.centeredView}>
                    <TouchableOpacity 
                        activeOpacity={0.5}
                        onPress={closeModalFunc}
                        style={{ position: 'absolute', top: 20, right: 20, height: 50, width: 50, backgroundColor: colors.light_gray, borderRadius: 25, alignItems: 'center', justifyContent: 'center'}} >
                        <Image source={media.close} style={{height: 18, width: 18}} />
                    </TouchableOpacity>
                    <ScrollView style={styles.modalView}>
                        <Text style={styles.modalText}>Promo Codes</Text>

                        <View style={{marginBottom: 40}} >
                           {promoCodesData.map((item) => (
                            <TouchableOpacity
                                onPress={() => {
                                    dispatch(setPromoCodeValue(item.offer_code))
                                    closeModalFunc(); 
                                }}
                                >
                                <View style={{height: 120, backgroundColor: colors.primary, marginBottom: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'space-evenly', flexDirection: 'row'}} >
                                    <Text style={{fontSize: fontSize.Title, color: 'white', fontWeight: 'bold' }} >{item.offer_percent}</Text>
                                    <View style={{
                                        borderStyle: 'dashed',
                                        borderWidth: 1,
                                        borderRadius: 1,
                                        height: 70,
                                        borderColor: 'white'
                                    }}>
                                    </View>
                                    <Text style={{fontSize: fontSize.SubTitle, color: 'white', fontWeight: 'bold' }} >{item.offer_code}</Text>
                                
                                </View>

                                
                                <View style={{position: 'absolute',top: -15, height: 150, width: '100%', alignItems: 'center', justifyContent: 'space-between', }} >
                                    <View style={{ height: 30, width: 30, borderRadius: 15, marginLeft: 10,  backgroundColor: 'white'}} />
                                    <View style={{ height: 30, width: 30, borderRadius: 15, marginLeft: 10,  backgroundColor: 'white'}} />
                                </View>
                            </TouchableOpacity>
                           ))}
                        </View>
                    </ScrollView>
                </View>
            </Modal>
        </View>
    )
}


const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      //justifyContent: 'center',
      alignItems: 'center',
      marginTop: 40,
      backgroundColor: 'white'
    },
    modalView: {
      padding: 20,
      marginTop: 70,
      width: screenWidth-40,
      backgroundColor: 'white'
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
    },
    buttonOpen: {
      backgroundColor: '#F194FF',
    },
    buttonClose: {
      backgroundColor: '#2196F3',
    },
    textStyle: {
      color: 'white',
      fontWeight: 'bold',
      textAlign: 'center',
    },
    modalText: {
      marginBottom: 15,
      textAlign: 'center',
      fontSize: fontSize.Heading,
      color: colors.dark_blue,
      fontWeight: 'bold'
    },
  });

export default PromoCodes
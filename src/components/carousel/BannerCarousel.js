import React, {useState, useRef} from 'react';
import {Text, View, Dimensions, Image} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import { screenWidth } from '../../global/constants';
import { colors } from '../../global/colors';

//export const SLIDER_WIDTH = Dimensions.get('window').width + 10;
export const SLIDER_WIDTH = screenWidth
export const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.8);

const data = [
    {
        id: 1,
        image: require('../../assets/images/banner/banner1.png'),
        name: 'React JS',
        url: 'https://icon-library.com/images/react-icon/react-icon-29.jpg',
    },
    {
        id: 2,
        image: require('../../assets/images/banner/banner2.png'),
        name: 'JavaScript',
        url: 'https://upload.wikimedia.org/wikipedia/commons/3/3b/Javascript_Logo.png',
    },
    {
        id: 3,
        image: require('../../assets/images/banner/banner3.png'),
        name: 'Node JS',
        url: 'https://upload.wikimedia.org/wikipedia/commons/6/67/NodeJS.png',
    },
    {
        id: 4,
        image: require('../../assets/images/banner/banner4.png'),
        name: 'JavaScript',
        url: 'https://upload.wikimedia.org/wikipedia/commons/3/3b/Javascript_Logo.png',
    },
    {
        id: 5,
        image: require('../../assets/images/banner/banner5.png'),
        name: 'Node JS',
        url: 'https://upload.wikimedia.org/wikipedia/commons/6/67/NodeJS.png',
    },
];

const renderItem = ({item}) => {
    return (
        <View
            style={{
                borderWidth: 1,
                borderRadius: 20,
                height: 152,
                alignItems: 'center',
                backgroundColor: 'white',
                borderColor: colors.dark_blue,
            }}>
            <Image source={item.image} style={{width: '100%', height: 150, resizeMode: 'stretch', borderRadius: 20}} />
        </View>
    );
};

const BannerCarousel = () => {
    const [index, setIndex] = useState(0);
    const isCarousel = useRef(null);
    return (
        <View style={{marginVertical: 10, marginBottom: 0}}>
            <Carousel
                loop={true}
                autoplay={true}
                ref={isCarousel}
                data={data}
                renderItem={renderItem}
                sliderWidth={SLIDER_WIDTH}
                itemWidth={ITEM_WIDTH}
                onSnapToItem={index => setIndex(index)}
            />
            <Pagination

                dotsLength={data.length}
                activeDotIndex={index}
                carouselRef={isCarousel}
                dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                marginHorizontal: 8,
                backgroundColor: colors.dark_blue,
                }}
                tappableDots={true}
                inactiveDotStyle={{
                backgroundColor: 'black',
                // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        </View>
    );
};


export default BannerCarousel
import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  
const BackButton = ({ title, icon, onPress, navigation, header}) => {
    return (
        <>
            <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {onPress ? onPress() : navigation.goBack()}}
                    style={[styles.container, {marginRight: header == 'headerRight' ? 20 : 0} ]}
                    >
                    <Image source={icon ? icon : media.left_arrow_white} style={{height: 28, width: 28}} />
                </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: colors.dark_blue,
        marginLeft: 20
    },
    outlinedContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        height: 60,
        borderRadius: 10,
        borderColor: colors.dark_blue,
        borderWidth: 2.5,
    },
    title: {
        fontSize: fontSize.Title,
        fontWeight: 'bold',
        textAlign: 'center',
        color: colors.dark_blue,
    },
    skipText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        textAlign: 'center',
        color: colors.dark_blue,
        textDecorationLine: 'underline',
        marginVertical: 10,
        marginRight: 10,
    },
})

export default BackButton

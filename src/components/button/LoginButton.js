import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  
const LoginButton = ({ title, onPress, isOutlined, skipButton, disabled, icon }) => {
    return (
        <>
            {skipButton
                ?
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={onPress}
                    disabled={disabled}
                    >
                    <Text style={styles.skipText} >Skip</Text>
                </TouchableOpacity>
                :
                <View>
                    {icon
                        ?
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            onPress={onPress}
                            disabled={disabled}
                            style={isOutlined ? styles.outlinedContainer : [styles.container, {backgroundColor: disabled ? '#d3d3d390' : colors.dark_blue, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30}]} >
                            <Text style={styles.title} >{title}</Text>
                            <Image source={icon} style={{height: 38, width: 28}} />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            onPress={onPress}
                            disabled={disabled}
                            style={isOutlined ? styles.outlinedContainer : [styles.container, {backgroundColor: disabled ? '#d3d3d390' : colors.dark_blue,}]} >
                            <Text style={[styles.title, isOutlined ? {color: colors.dark_blue} : {}]} >{title}</Text>
                        </TouchableOpacity>

                    }
                </View>
            }
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        height: 60,
        borderRadius: 30,
        marginVertical: 10,
        marginBottom: 0,
    },
    outlinedContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        height: 60,
        borderRadius: 30,
        borderColor: colors.dark_blue,
        borderWidth: 1.5,
    },
    title: {
        fontSize: fontSize.SubTitle,
        fontWeight: 'bold',
        textAlign: 'center',
        color: colors.white,
    },
    skipText: {
        fontSize: fontSize.SubTitle,
        fontWeight: '500',
        textAlign: 'center',
        color: colors.dark_blue,
        textDecorationLine: 'underline',
        marginVertical: 10,
    },
})

export default LoginButton

export const media = {
    wave: require('../assets/images/wave.png'),

    empty_cart: require('../assets/images/illustrations/empty_cart.png'),
    empty_wishlist: require('../assets/images/illustrations/empty_wishlist.png'),
    empty_notification: require('../assets/images/illustrations/empty_notification.png'),
    
    
    category_home: require('../assets/images/categories/home.png'),
    category_furniture: require('../assets/images/categories/furniture.png'),
   

    category_appliances: require('../assets/images/categories/appliances.png'),
    category_beauty: require('../assets/images/categories/beauty.png'),
    category_electronics: require('../assets/images/categories/electronics.png'),

    category_mobile: require('../assets/images/categories/mobile.png'),
    category_fashion: require('../assets/images/categories/fashion.png'),
    category_all: require('../assets/images/categories/menu.png'),

    plus: require('../assets/images/plus.png'),

    bags: require('../assets/images/bags.png'),


    close: require('../assets/images/close.png'),

    booking_outlined: require('../assets/images/booking_outlined.png'),
    location_outlined: require('../assets/images/location_outlined.png'),
    wallet_outlined: require('../assets/images/wallet_outlined.png'),

    booking: require('../assets/images/booking.png'),
    location: require('../assets/images/location.png'),
    wallet: require('../assets/images/wallet.png'),

   
    home_fill: require('../assets/images/tabbar/home_fill.png'),
    user_fill: require('../assets/images/tabbar/user_fill.png'),
    cart_fill: require('../assets/images/tabbar/cart_fill.png'),
    bookmark_fill: require('../assets/images/tabbar/bookmark_fill.png'),
    categories_fill: require('../assets/images/tabbar/categories_fill.png'),
    admin_fill: require('../assets/images/tabbar/admin_fill.png'),

    home: require('../assets/images/tabbar/home.png'),
    user: require('../assets/images/tabbar/user.png'),
    cart: require('../assets/images/tabbar/cart.png'),
    bookmark: require('../assets/images/tabbar/bookmark.png'),
    categories: require('../assets/images/tabbar/categories.png'),
    admin: require('../assets/images/tabbar/admin.png'),


    right_arrow_small: require('../assets/images/right-arrow.png'),
    
    bookmark_blue: require('../assets/images/bookmark_blue.png'),
    location_blue: require('../assets/images/location_blue.png'),
    user_blue: require('../assets/images/user_blue.png'),
    edit_blue: require('../assets/images/edit_blue.png'),
    bell_blue: require('../assets/images/bell_blue.png'),
    logout_blue: require('../assets/images/logout_blue.png'),


    discount: require('../assets/images/discount.png'),


    padlock: require('../assets/images/padlock.png'),
    email: require('../assets/images/email.png'),

    buyer: require('../assets/images/buyer.png'),
    seller: require('../assets/images/seller.png'),

    search: require('../assets/images/search.png'),
    bell: require('../assets/images/bell.png'),

    star: require('../assets/images/star.png'),


    hide: require('../assets/images/hide.png'),
    view: require('../assets/images/view.png'),
    menu: require('../assets/images/menu.png'),
    delete: require('../assets/images/delete.png'),
    filter: require('../assets/images/filter.png'),
    refresh: require('../assets/images/refresh.png'),
    heart_outline: require('../assets/images/heart_outline.png'),
    heart_fill: require('../assets/images/heart_fill.png'),

    log_out: require('../assets/images/log_out.png'),
    right_arrow: require('../assets/images/right_arrow.png'),
    right_arrow_white: require('../assets/images/right_arrow_white.png'),
    left_arrow_white: require('../assets/images/left_arrow_white.png'),



    app_logo: require('../assets/images/app_logo.png'),
    app_logo_white: require('../assets/images/app_logo_white.png'),

    // shopping: require('../assets/images/shopping.png'),
    // empty_wishlist: require('../assets/images/empty_wishlist.png'),
    // empty_cart: require('../assets/images/empty_cart.png'),
    // order_confirmed: require('../assets/images/order_confirmed.png'),
    // deliveries: require('../assets/images/deliveries.png'),
    // empty_wishlist: require('../assets/images/empty_wishlist.png'),
    // add_to_cart: require('../assets/images/add_to_cart.png'),
    // successful_purchase: require('../assets/images/successful_purchase.png'),
    // shopping_app: require('../assets/images/shopping_app.png'),

    
}

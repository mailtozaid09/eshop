import { colors } from "./colors";
import { media } from "./media";

export const sampleData = [
    {
        id: 1
    },
    {
        id: 2
    },
]


export const suggestions_data = [
    {
        id: 1,
        title: 'T-shirt',
    },
    {
        id: 2,
        title: 'Jeans',
    },
    {
        id: 3,
        title: 'Shoes',
    },
    {
        id: 4,
        title: 'Jacket',
    },
    {
        id: 5,
        title: 'Washing Machine',
    },
    {
        id: 6,
        title: 'Juicer',
    },
    {
        id: 7,
        title: 'Headphones',
    },
]


export const promoCodesData = [
    {
        id: 1,
        image_url: '',
        offer_code: 'ESHOP25',
        offer_percent: '25% OFF',
    },
    {
        id: 2,
        image_url: '',
        offer_code: 'ESHOP40',
        offer_percent: '40% OFF',
    },
    {
        id: 3,
        image_url: '',
        offer_code: 'ESHOP35',
        offer_percent: '35% OFF',
    },
    {
        id: 4,
        image_url: '',
        offer_code: 'ESHOP15',
        offer_percent: '15% OFF',
    },
    {
        id: 5,
        image_url: '',
        offer_code: 'ESHOP25',
        offer_percent: '25% OFF',
    },
    {
        id: 6,
        image_url: '',
        offer_code: 'ESHOP50',
        offer_percent: '50% OFF',
    },
]

export const categoriesData = [
    {
        id: 1,
        categoryIcon: media.category_all,
        category: 'All',
    },
    {
        id: 2,
        categoryIcon: media.category_home,
        category: 'Home',
    },
    {
        id: 3,
        categoryIcon: media.category_fashion,
        category: 'Fashion',
    },
    {
        id: 4,
        categoryIcon: media.category_mobile,
        category: 'Mobiles',
    },
    {
        id: 5,
        categoryIcon: media.category_electronics,
        category: 'Electronics',
    },
    {
        id: 6,
        categoryIcon: media.category_beauty,
        category: 'Beauty',
    },
    {
        id: 7,
        categoryIcon: media.category_furniture,
        category: 'Furniture',
    },
    {
        id: 8,
        categoryIcon: media.category_appliances,
        category: 'Appliances',
    },
]

export const shoppingListData = [
    {
        "image_url": 'https://m.media-amazon.com/images/I/51usnGwoELL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Fashion",
        "item_name": "Men's Classic T-Shirt",
        "item_description": "A comfortable and stylish t-shirt for men.",
        "price": 1499,
        "ratings": 4.5,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61jgv5MirGL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Fashion",
        "item_name": "Women's Skinny Jeans",
        "item_description": "Trendy skinny jeans for women with a perfect fit.",
        "price": 4999,
        "ratings": 4.3,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/51mP0GQRo+L._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Fashion",
        "item_name": "Men's Leather Jacket",
        "item_description": "A stylish and durable leather jacket for men.",
        "price": 1399,
        "ratings": 4.7,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/91qW94rNpJL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Fashion",
        "item_name": "Women's Floral Dress",
        "item_description": "A beautiful floral dress for women perfect for special occasions.",
        "price": 799,
        "ratings": 4.8,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61XzHEzuJxL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Fashion",
        "item_name": "Men's Athletic Shoes",
        "item_description": "High-quality athletic shoes for men with great comfort and support.",
        "price": 1490,
        "ratings": 4.6,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/21V+Fpk2TJL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Fashion",
        "item_name": "Women's Yoga Leggings",
        "item_description": "Comfortable and stretchy leggings for women perfect for yoga and exercise.",
        "price": 999,
        "ratings": 4.4,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61DGAlvxRLL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Fashion",
        "item_name": "Men's Plaid Shirt",
        "item_description": "A classic plaid shirt for men with a timeless style.",
        "price": 679,
        "ratings": 4.2,
    },


    
    



    {
        "image_url": 'https://m.media-amazon.com/images/I/81LT9vdwUNL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Sofa",
        "item_description": "A comfortable sofa with modern design.",
        "ratings": 4.5,
        "price": 20499,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/81NNK+KY0sL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Dining Table",
        "item_description": "A sturdy dining table with ample seating.",
        "ratings": 4.2,
        "price": 14299,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61F-ogjg58L._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Bed",
        "item_description": "A cozy bed with a headboard and storage drawers.",
        "ratings": 4.8,
        "price": 22597,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/81SAwzNdaVL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Coffee Table",
        "item_description": "A minimalist coffee table with a glass top.",
        "ratings": 4.0,
        "price": 3196,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/81yXdUSEh+L._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Curtains",
        "item_description": "Elegant curtains with blackout lining.",
        "ratings": 4.3,
        "price": 1191,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71TWqgp-zHL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Bookshelf",
        "item_description": "A tall bookshelf with adjustable shelves.",
        "ratings": 4.6,
        "price": 3999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71s2+AVKC7L._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Rug",
        "item_description": "A plush rug with a geometric pattern.",
        "ratings": 4.1,
        "price": 499,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/81ZwgIIxBCL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Wall Art",
        "item_description": "An abstract wall art piece for your living room.",
        "ratings": 4.7,
        "price": 1799,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/81nij8Qz2vL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Lamp",
        "item_description": "A stylish lamp with a brass base and linen shade.",
        "ratings": 4.4,
        "price": 1299,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/81f8kWOvmiL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Throw Pillow",
        "item_description": "A soft throw pillow with a geometric pattern.",
        "ratings": 4.0,
        "price": 379,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71VR-NjNKYL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Home",
        "item_name": "Duvet Cover",
        "item_description": "A luxurious duvet cover made from premium cotton.",
        "ratings": 4.5,
        "price": 798,
    },





    {
        "image_url": 'https://m.media-amazon.com/images/I/61S9qLsOPPL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Refrigerator",
        "item_description": "A large appliance for preserving food at low temperatures.",
        "ratings": 4.5,
        "price": 17590,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71RgbTPN1GL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Washing Machine",
        "item_description": "An appliance for cleaning clothes by using water and detergent.",
        "ratings": 4.2,
        "price": 18990,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71QzNXKXwdL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Microwave Oven",
        "item_description": "An appliance for cooking or heating food using microwave radiation.",
        "ratings": 4.0,
        "price": 5999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/51k0NvUmYLL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Air Conditioner",
        "item_description": "An appliance for cooling or heating the air in a room or building.",
        "ratings": 4.8,
        "price": 28999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/41E5HOabWWS._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Dishwasher",
        "item_description": "An appliance for cleaning dishes automatically.",
        "ratings": 4.3,
        "price": 14990,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/51JBzLQxczL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Toaster",
        "item_description": "An appliance for browning slices of bread or other food items.",
        "ratings": 4.1,
        "price": 2095,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61iKvcMlvgL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Coffee Maker",
        "item_description": "An appliance for brewing coffee.",
        "ratings": 4.4,
        "price": 1599,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61VsIozUNLL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Blender",
        "item_description": "An appliance for mixing or pureeing food items.",
        "ratings": 4.0,
        "price": 2999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71EqgKLhTtL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Juicer",
        "item_description": "An appliance for extracting juice from fruits and vegetables.",
        "ratings": 4.3,
        "price": 21900,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/51EGS1jWNeS._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Electric Kettle",
        "item_description": "An appliance for boiling water for tea, coffee, or other hot beverages.",
        "ratings": 4.2,
        "price": 1549,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/51k9Necv-NL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Appliances",
        "item_name": "Vacuum Cleaner",
        "item_description": "An appliance for cleaning floors and carpets by suction.",
        "ratings": 4.6,
        "price": 6199,
    },






    {
        "image_url": 'https://m.media-amazon.com/images/I/61cMQeVEGKL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Mobiles",
        "item_name": "iPhone 13",
        "item_description": "The latest iPhone with a powerful A15 Bionic chip, dual-camera system, and advanced OLED Super Retina XDR display.",
        "ratings": 4.8,
        "price": 89999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71qZERyxy6L._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Mobiles",
        "item_name": "Samsung Galaxy S21",
        "item_description": "A flagship Android smartphone with a high-quality camera system, powerful Snapdragon 888 processor, and large AMOLED display.",
        "ratings": 4.7,
        "price": 57989,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71QQZr2pNSL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Mobiles",
        "item_name": "Google Pixel 6",
        "item_description": "The latest Pixel smartphone with a custom Google Tensor chip, advanced camera features, and pure Android experience.",
        "ratings": 4.9,
        "price": 38490,      
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/617MPEZB5mL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Mobiles",
        "item_name": "OnePlus 9 Pro",
        "item_description": "A premium Android smartphone with a powerful Snapdragon 888 processor, Hasselblad-tuned camera system, and fast charging technology.",
        "ratings": 4.6,
        "price": 28999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61A0Zu4K-TL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Mobiles",
        "item_name": "Apple iPhone SE",
        "item_description": "A budget-friendly iPhone with a powerful A14 Bionic chip, compact size, and advanced camera capabilities.",
        "ratings": 4.5,
        "price": 54990,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71yTvU9VgdL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Mobiles",
        "item_name": "Xiaomi Mi 11",
        "item_description": "A flagship Android smartphone with a powerful Snapdragon 888 processor, high-quality camera system, and large AMOLED display.",
        "ratings": 4.7,
        "price": 12999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/51ys+7E3ojL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Mobiles",
        "item_name": "Sony Xperia 1 III",
        "item_description": "A premium Android smartphone with a high-quality camera system, powerful Snapdragon 888 processor, and 4K OLED display.",
        "ratings": 4.8,
        "price": 38999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61VbKHdE0rL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Mobiles",
        "item_name": "LG Velvet",
        "item_description": "A stylish Android smartphone with a unique design, powerful Snapdragon 765G processor, and versatile camera system.",
        "ratings": 4.5,
        "price": 15499,
    },







    {
        "image_url": 'https://m.media-amazon.com/images/I/51wPWj--fAL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Electronics",
        "item_name": "Sony PlayStation 5",
        "item_description": "A next-generation gaming console with ultra-high-speed SSD, ray tracing, and haptic feedback for an immersive gaming experience.",
        "ratings": 4.9,
        "price": 39890,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61sPE6CnkVL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Electronics",
        "item_name": "Samsung 65-inch 4K Smart TV",
        "item_description": "A high-quality smart TV with 4K resolution, HDR support, and smart features like built-in streaming apps and voice control.",
        "ratings": 4.7,
        "price": 45990,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/81cJ1kUT1LL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Electronics",
        "item_name": "Zebronics Headphones",
        "item_description": "Premium noise-cancelling headphones with top-notch sound quality, comfortable fit, and up to 20 hours of battery life.",
        "ratings": 4.8,
        "price": 699,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61L5QgPvgqL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Electronics",
        "item_name": "Apple MacBook Pro",
        "item_description": "A high-performance laptop with a powerful M1 chip, stunning Retina display, and long battery life for professional use.",
        "ratings": 4.9,
        "price": 129990,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/81cEKnH692L._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Electronics",
        "item_name": "Canon EOS R6 Mirrorless Camera",
        "item_description": "A professional-grade mirrorless camera with a high-resolution sensor, advanced autofocus, and 4K video capabilities.",
        "ratings": 4.8,
        "price": 2499.99,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61ZXwnqqOuS._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Electronics",
        "item_name": "Fitbit Versa 3 Smartwatch",
        "item_description": "A feature-rich smartwatch with built-in GPS, heart rate tracking, sleep tracking, and music playback for active lifestyle.",
        "ratings": 4.6,
        "price": 18689,
    },

    {
        "image_url": 'https://m.media-amazon.com/images/I/61JhG2HKMCL._AC_UY436_FMwebp_QL65_.jpg',
        "category": "Electronics",
        "item_name": "Amazon Echo Dot",
        "item_description": "A compact smart speaker with Alexa voice control, built-in Bluetooth, and versatile smart home capabilities.",
        "ratings": 4.5,
        "price": 4999,
    },






    {
        "image_url": 'https://m.media-amazon.com/images/I/41xSVcLjwyL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Furniture",
        "item_name": "Accent Chair",
        "item_description": "A stylish accent chair with a comfortable cushion and a unique design to add a touch of elegance to your home.",
        "ratings": 4.8,
        "price": 1999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71KnTqgVOFL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Furniture",
        "item_name": "Dresser",
        "item_description": "A spacious and functional dresser with multiple drawers for storing your clothes and accessories.",
        "ratings": 4.6,
        "price": 16999,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/612YrUGJmiL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Furniture",
        "item_name": "Office Desk",
        "item_description": "A functional and stylish office desk with ample workspace and storage options for your home office.",
        "ratings": 4.7,
        "price": 3499,
    },






    {
        "image_url": 'https://m.media-amazon.com/images/I/41t3uS9z7QL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Beauty",
        "item_name": "NARS Radiant Creamy Concealer",
        "item_description": "A creamy concealer that provides buildable coverage and brightens the under-eye area, leaving a natural-looking finish.",
        "ratings": 4.7,
        "price": 1100,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/41ocH1HeRfL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Beauty",
        "item_name": "MAC Ruby Woo Lipstick",
        "item_description": "A classic matte red lipstick with a long-lasting formula and intense color payoff, perfect for a bold lip look.",
        "ratings": 4.8,
        "price": 559,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/61w3L747mUL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Beauty",
        "item_name": "Urban Decay Naked3 Eyeshadow Palette",
        "item_description": "A versatile eyeshadow palette with 12 rose-hued shades in matte, shimmer, and metallic finishes, perfect for creating a variety of looks.",
        "ratings": 4.6,
        "price": 1400,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/41MJ2F9Cb7L._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Beauty",
        "item_name": "Estee Lauder Double Wear Stay-in-Place Foundation",
        "item_description": "A long-lasting, full-coverage foundation with a matte finish that stays put for up to 24 hours, providing a flawless complexion.",
        "ratings": 4.9,
        "price": 219,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/51BCmVm76XL._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Beauty",
        "item_name": "Tatcha The Water Cream",
        "item_description": "A lightweight, oil-free moisturizer with Japanese botanicals that provides hydration and helps to refine skin texture and reduce pore appearance.",
        "ratings": 4.7,
        "price": 8949,
    },
    {
        "image_url": 'https://m.media-amazon.com/images/I/71KtlPImW3L._AC_UL800_FMwebp_QL65_.jpg',
        "category": "Beauty",
        "item_name": "Clinique Take The Day Off Cleansing Balm",
        "item_description": "A gentle cleansing balm that melts away makeup, dirt, and impurities without stripping the skin, leaving it clean and hydrated.",
        "ratings": 4.6,
        "price": 920,
    },
]


export const onboarding_data = [
    {
        id: 1,
        image: media.shopping,
        title: 'Explore the New World',
        description: 'Find your best clothing option without any delay'
    },
    {
        id: 2,
        image: media.shopping_app,
        title: 'Discover New Trends',
        description: 'We provide a broad e-commerce app for our special cutomers'
    },
    {
        id: 3,
        image: media.deliveries,
        title: 'Get Faster Delivery',
        description: 'Fast delivery within 20 minutes to your home or office'
    },
]

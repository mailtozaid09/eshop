import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';


import { colors } from '../../global/colors';
import { media } from '../../global/media';
import { fontSize } from '../../global/fontFamily';
import HomeStack from '../home';

import AdminScreen from '../../screens/admin';
import HomeHeader from '../../components/header/HomeHeader';
import { screenWidth } from '../../global/constants';
import CartStack from '../cart';
import ProfileStack from '../profile';


const Tab = createBottomTabNavigator();


export default function Tabbar() {

    LogBox.ignoreAllLogs(true)
    


    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = 22, width = 22 ;
    
                if (route.name === 'HomeStack') {
                    iconName = focused ? media.home_fill : media.home
                    routeName = 'Home' 
                } 
                else if (route.name === 'CartStack') {
                    iconName = focused ? media.cart_fill : media.cart
                    routeName = 'Cart' 
                } else if (route.name === 'ProfileStack') {
                    iconName = focused ? media.user_blue : media.user
                    routeName = 'Profile' 
                }
                else if (route.name === 'Admin') {
                    iconName = focused ? media.admin_fill : media.admin
                    routeName = 'Admin' 
                    height = 26;
                    width = 26 ;
                } 
                return(
                    <View style={[{alignItems: 'center', justifyContent: 'center', height: 44, padding: 12, paddingHorizontal: 15, borderRadius: 22, flexDirection: 'row' }, focused ? {backgroundColor:  colors.white ,} : null ]} >
                        <Image source={iconName} style={{height: height, width: width, marginRight: 10, resizeMode: 'contain'}}/>
                        {focused && <Text style={{fontSize: 14, color: colors.dark_blue, fontWeight: '600'}} >{routeName}</Text>}
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    headerTitle: ' ',
                    headerLeft: () =>  (<HomeHeader navigation={navigation} />),
                    headerRight: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'ShoppingItemDetails' || routeName === 'Search' || routeName === 'Notifications') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            <Tab.Screen
                name="CartStack"
                component={CartStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    headerTitle: 'Cart',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'Checkout') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            {/* <Tab.Screen
                name="Admin"
                component={AdminScreen}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'Checkout') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            /> */}
            <Tab.Screen
                name="ProfileStack"
                component={ProfileStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'Wishlist') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
        </Tab.Navigator>
    );
}



const styles = StyleSheet.create({
    tabbarStyle: {
        height: 80, 
        
        paddingHorizontal: 12, 
        paddingTop: 15, 
        borderTopWidth: 2, 
        borderColor: colors.dark_blue, 
        backgroundColor: colors.dark_blue
    }
})
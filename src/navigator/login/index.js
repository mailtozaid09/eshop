import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import OnBoardingScreen from '../../screens/onboarding';
import LoginScreen from '../../screens/login';
import SignUpScreen from '../../screens/signup';
import WhoAreYouScreen from '../../screens/signup/WhoAreYouScreen';
import SettingUpScreen from '../../screens/signup/SettingUpScreen';




const Stack = createStackNavigator();


const LoginStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Login" 
        >
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="SignUp"
                component={SignUpScreen}
                options={{
                    headerShown: false
                }}
            />
             <Stack.Screen
                name="WhoAreYou"
                component={WhoAreYouScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="SettingUp"
                component={SettingUpScreen}
                options={{
                    headerShown: false
                }}
            />


        </Stack.Navigator>
    );
}

export default LoginStack
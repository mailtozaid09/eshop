import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../../screens/home';
import ShoppingItemDetails from '../../screens/home/ShoppingItemDetails';
import HomeHeader from '../../components/header/HomeHeader';
import BackButton from '../../components/button/BackButton';
import { media } from '../../global/media';
import SearchScreen from '../../screens/home/SearchScreen';
import NotificationsScreen from '../../screens/home/NotificationsScreen';




const Stack = createStackNavigator();


const HomeStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: true,
                    headerTitle: '',
                    headerRight: () => null,
                    headerLeft: () =>  (<HomeHeader navigation={navigation} />),
                }}
            />
            <Stack.Screen
                name="ShoppingItemDetails"
                component={ShoppingItemDetails}
                options={{
                    headerShown: true,
                    headerTitle: '',
                    headerRight: () => (<BackButton header='headerRight' navigation={navigation} icon={media.cart} onPress={() => {navigation.navigate('Home'); navigation.navigate('Cart')}} />),
                    headerLeft: () =>  (<BackButton navigation={navigation} />),
                }}
            />
            <Stack.Screen
                name="Search"
                component={SearchScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Search',
                    headerRight: () => null,
                    headerLeft: () =>  (<BackButton navigation={navigation} />),
                }}
            />
            <Stack.Screen
                name="Notifications"
                component={NotificationsScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Notifications',
                    headerRight: () => null,
                    headerLeft: () =>  (<BackButton navigation={navigation} />),
                }}
            />

        </Stack.Navigator>
    );
}

export default HomeStack
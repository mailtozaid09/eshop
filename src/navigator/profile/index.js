import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import Checkout from '../../screens/cart/Checkout';
import CartScreen from '../../screens/cart';
import { fontSize } from '../../global/fontFamily';
import BackButton from '../../components/button/BackButton';
import OrderConfirmation from '../../screens/orders/OrderConfirmation';
import ProfileScreen from '../../screens/profile';
import WishlistScreen from '../../screens/profile/WishlistScreen';
import EditProfile from '../../screens/profile/EditProfile';




const Stack = createStackNavigator();


const ProfileStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Profile" 
        >
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Profile',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            <Stack.Screen
                name="EditProfile"
                component={EditProfile}
                options={{
                    headerShown: true,
                    headerTitle: 'Profile',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            
            <Stack.Screen
                name="Wishlist"
                component={WishlistScreen}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitle: 'Wishlist',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => (<BackButton navigation={navigation} />),
                })}
            />
        </Stack.Navigator>
    );
}

export default ProfileStack
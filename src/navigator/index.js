import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/home';
import OnboardingStack from './onboarding';
import LoginStack from './login';
import Tabbar from './tabbar';
//import { createDrawerNavigator } from '@react-navigation/drawer';



const Stack = createStackNavigator();
// const Drawer = createDrawerNavigator();

const Navigator = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Tabbar" 
        >
            <Stack.Screen
                name="OnboardingStack"
                component={OnboardingStack}
                options={{
                    headerShown: false
                }}
            /> 
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Tabbar"
                component={Tabbar}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    );
}

export default Navigator
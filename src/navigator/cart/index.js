import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import Checkout from '../../screens/cart/Checkout';
import CartScreen from '../../screens/cart';
import { fontSize } from '../../global/fontFamily';
import BackButton from '../../components/button/BackButton';
import OrderConfirmation from '../../screens/orders/OrderConfirmation';




const Stack = createStackNavigator();


const CartStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Cart" 
        >
            <Stack.Screen
                name="Cart"
                component={CartScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Cart',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            <Stack.Screen
                name="Checkout"
                component={Checkout}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitle: 'Checkout',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => (<BackButton navigation={navigation} />),
                })}
            />
            <Stack.Screen
                name="OrderConfirmation"
                component={OrderConfirmation}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitle: 'Order Confirmation',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => (<BackButton navigation={navigation} onPress={() => navigation.navigate('Cart')} />),
                })}
            />

        </Stack.Navigator>
    );
}

export default CartStack
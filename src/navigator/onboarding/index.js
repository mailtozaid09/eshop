import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import OnBoardingScreen from '../../screens/onboarding';
import WelcomeScreen from '../../screens/onboarding/WelcomeScreen';




const Stack = createStackNavigator();


const OnboardingStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="WelcomeScreen" 
        >
            <Stack.Screen
                name="Welcome"
                component={WelcomeScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="OnBoarding"
                component={OnBoardingScreen}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    );
}

export default OnboardingStack
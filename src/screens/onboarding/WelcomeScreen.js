import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image } from 'react-native'
import { media } from '../../global/media'
import { fontSize } from '../../global/fontFamily'
import { colors } from '../../global/colors'
import LoginButton from '../../components/button/LoginButton'
  


const WelcomeScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >               
                <View style={styles.textContainer} >

                    <Image source={media.app_logo_white} style={styles.mainImg} />

                    <Text style={styles.title} >WELCOME</Text>
                    <Text style={styles.subtitle}>Let's make your shopping now</Text>
                </View>

                <LoginButton
                    title="START SHOPPING"
                    icon={media.right_arrow_white}
                    onPress={() => {navigation.navigate('OnBoarding')}}
                />

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white
    },
    title: {
        fontSize: fontSize.Heading,
        color: colors.dark_blue,
        fontWeight: 'bold',
    },
    subtitle: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '600',
        marginTop: 20,
        textAlign: 'center'
    },
    textContainer: {
        flex: 1,
        paddingBottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainImg: {
        width: 250, 
        height: 250, 
        borderRadius: 20,
        margin: 20,
        marginTop: 0,
        marginBottom: 50,
        // resizeMode: 'contain',
    },
})

export default WelcomeScreen
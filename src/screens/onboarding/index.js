import React, { useState } from 'react'
import { Text, View, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import TextButton from '../../components/button/TextButton'
import HeaderContainer from '../../components/onboarding/HeaderContainer'

import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
import { onboarding_data } from '../../global/sampleData'
  
const OnBoardingScreen = ({navigation}) => {

    const [activeId, setActiveId] = useState(1);

    const arrowButton = (value) => {
        if(value == 'right'){
            if(activeId < 3){
                setActiveId(prev => prev + 1)
            }else{
                navigation.navigate('LoginStack', {screen: 'Login'})
            }
        }else{
            if(activeId > 1){
                setActiveId(prev => prev - 1)
            }else{
                navigation.navigate('Welcome')
            }
            
        }
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >

                
                <View>
                    {onboarding_data.map((item, index) => (
                        
                            <View>
                                {item.id == activeId ? <View style={styles.imageContainer} >
                                    <Image source={item.image} style={styles.mainImg} />
                                
                                    <View style={styles.textContainer} >
                                        <Text style={styles.title} >{item.title}</Text>
                                        <Text style={styles.subtitle}>{item.description}</Text>
                                    </View>
                                </View>
                                : null}
                            </View>
                   

                    ))}
               </View>

               <View style={{flexDirection: 'row'}} >
                    {onboarding_data.map((item, index) => (
                        <View style={item.id == activeId ? styles.activeBar : styles.inactiveBar} >
                            
                        </View>
                    ))}
                </View>

                <View style={styles.bottomContainer} >
               
                    <TouchableOpacity 
                        onPress={() => arrowButton('left')}
                        activeOpacity={0.5}
                        style={styles.arrowButton} >
                        <Image source={media.left_arrow_white} style={styles.right_arrow} />
                    </TouchableOpacity>

                    {/* <TextButton title="Skip"  onPress={() => {}} /> */}
                    
                    

                    <TouchableOpacity 
                        onPress={() => arrowButton('right')}
                        activeOpacity={0.5}
                        style={styles.arrowButton} >
                        <Image source={media.right_arrow_white} style={styles.right_arrow} />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    imageContainer: {
        alignItems: 'center'
    },
    title: {
        fontSize: fontSize.Heading,
        color: colors.dark_blue,
        fontWeight: 'bold',
    },
    subtitle: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '600',
        marginTop: 20,
        textAlign: 'center'
    },
    textContainer: {
        padding: 20,
        marginTop: 10,
        paddingBottom: 0,
        alignItems: 'center'
    },
    mainImg: {
        width: 300, 
        height: 300, 
        borderRadius: 20,
        margin: 20,
        marginTop: 80,
        // resizeMode: 'contain',
    },
    bottom_arrow: {
        height: 150,
        width: screenWidth/1.5,
        resizeMode:'stretch'
    },
    right_arrow: {
        height: 38,
        width: 38
    },
    bottomContainer: {
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between', 
        width: screenWidth,
        paddingHorizontal: 20
    },
    arrowButton: {
        height: 60, 
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.dark_blue
    },
    activeBar: {
        height: 10, 
        width: 25, 
        marginRight: 10, 
        borderRadius: 6,
        backgroundColor: colors.dark_blue,
    },
    inactiveBar: {
        height: 10, 
        width: 10, 
        marginRight: 10, 
        borderRadius: 5,
        backgroundColor: colors.gray,
    },
})

export default OnBoardingScreen



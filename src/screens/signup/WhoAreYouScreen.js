import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Image } from 'react-native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'
import OutlinedInput from '../../components/input/OutlinedInput'
import LoginHeader from '../../components/custom/login/LoginHeader'
import LoginButton from '../../components/button/LoginButton'
import { fontSize } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'
  


const WhoAreYouScreen = ({navigation}) => {

    const [userDetails, setUserDetails] = useState('buyer');


    const continueFunc = () => {
       
        navigation.navigate('SettingUp')
    }


    return (
        <SafeAreaView style={styles.container} >
            

            <View style={{alignItems: 'center'}} >
                <LoginHeader 
                    title="Who Are You?"
                    subTitle="Please tell us a little bit more about yourself and who you are"
                />


                <TouchableOpacity
                    activeOpacity={0.5} 
                    onPress={() => setUserDetails('buyer')}
                    style={[styles.cardContainer, userDetails == 'buyer' ? {backgroundColor: colors.light_gray,  borderColor: colors.dark_blue} : {backgroundColor: '#fff',  borderColor: colors.light_gray}]} >
                    <Image source={media.buyer} style={styles.mainImg} />
                    <View style={{width: 200, marginVertical: 12, alignItems: 'center'}} >
                        <Text style={styles.title} >Buyer</Text>
                        <Text style={styles.subtitle} >You are planning to use this app as buyer</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    activeOpacity={0.5} 
                    onPress={() => setUserDetails('seller')}
                    style={[styles.cardContainer, userDetails == 'seller' ? {backgroundColor: colors.light_gray,  borderColor: colors.dark_blue} : {backgroundColor: '#fff',  borderColor: colors.light_gray}]} >
                    <Image source={media.seller} style={styles.mainImg} />
                    <View style={{width: 200, marginVertical: 12, alignItems: 'center'}} >
                        <Text style={styles.title} >Seller</Text>
                        <Text style={styles.subtitle} >You are planning to use this app as seller</Text>
                    </View>
                </TouchableOpacity>
             
            </View>
            
  
            
            <LoginButton title="CONTINUE" onPress={() => {continueFunc()}} />

              
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white
    },
    cardContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: 12,
        paddingHorizontal: 30,
        borderWidth: 2,
        width: screenWidth-40,
        marginBottom: 20,
        borderRadius: 20,
        backgroundColor: '#fff'
    },
    title: {
        fontSize: fontSize.SubHeading,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    subtitle: { 
        fontSize: 16,
        color: colors.dark_blue,
        fontWeight: '600',
        marginTop: 8,
        textAlign: 'center'
    },
    mainImg: {
        width: 70, 
        height: 70, 
        // borderRadius: 20,
        // margin: 30,
    },
})

export default WhoAreYouScreen
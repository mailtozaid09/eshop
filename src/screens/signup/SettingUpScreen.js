import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Image } from 'react-native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'
import OutlinedInput from '../../components/input/OutlinedInput'
import LoginHeader from '../../components/custom/login/LoginHeader'
import LoginButton from '../../components/button/LoginButton'
import { fontSize } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'
  


const SettingUpScreen = ({navigation}) => {

    useEffect(() => {
        setTimeout(() => {
            console.log('====================================');
            navigation.navigate('Tabbar')
            console.log('====================================');
        }, 5000);
    }, [])
    

    return (
        <SafeAreaView style={styles.container} >
            

          
                <Image source={media.app_logo} style={styles.mainImg} />
                
                <Text style={styles.title} >Setting up your Eshop...</Text>
        

              
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.dark_blue,
     
    },
    title: {
        fontSize: fontSize.Title,
        color: colors.white,
        fontWeight: '700',
    },
 
    mainImg: {
        width: 200, 
        height: 200, 
        margin: 60,
        marginTop: 200,
        marginBottom: 200
        // borderRadius: 20,
        // margin: 30,
    },
})

export default SettingUpScreen
import React, { useRef, useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, Modal, Pressable, SafeAreaView, ScrollView, } from 'react-native';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';

import { useDispatch, useSelector } from 'react-redux';

import LoginButton from '../../components/button/LoginButton';


const OrderConfirmation = ({ navigation}) => {
    
    return (
        <SafeAreaView style={styles.container} >
            <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', flex: 1,}} >
                <Image source={media.bags} style={{height: 200, width: 200, marginTop: 30}} />


                <Text style={styles.title} >Success!</Text>
                <Text style={styles.subtitle} >Your order will be delivered soon. {'\n'} Thank you for choosing our app!</Text>
            </View>

            <View style={{marginBottom: 15}} >
                <LoginButton 
                    title="TRACK YOUR ORDER"
                    isOutlined={true}
                    onPress={() => {navigation.navigate('Cart'); navigation.navigate('Home')}} 
                />
                <LoginButton 
                    title="CONTINUE SHOPING"
                    onPress={() => {navigation.navigate('Cart'); navigation.navigate('Home')}} 
                />
            </View>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent:'space-between',
        width: screenWidth,
        backgroundColor: colors.white,
        padding: 20,
    },
    title: {
        fontSize: fontSize.Heading,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 40,
        marginBottom: 12,
    },
    subtitle: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
        textAlign: 'center'
    },
    header: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 10,
        textAlign: 'center'
    },
});




export default OrderConfirmation
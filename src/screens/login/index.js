import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, TouchableOpacity, SafeAreaView, StyleSheet, Image } from 'react-native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'
import OutlinedInput from '../../components/input/OutlinedInput'
import LoginHeader from '../../components/custom/login/LoginHeader'
import LoginButton from '../../components/button/LoginButton'
import { fontSize } from '../../global/fontFamily'
  


const LoginScreen = ({navigation}) => {

    const [showEyeIcon, setShowEyeIcon] = useState(false);

    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


   
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const signUpFunc = () => {
        console.log("form => ", form);
        //navigation.navigate('ProfileStack', {screen: 'CreateProfile'})
    }


    return (
        <SafeAreaView style={styles.container} >
            

            <View>
                <LoginHeader 
                    title="Log In"
                    subTitle="Please provide us with this information in order to log in your account"
                />

             

                <OutlinedInput
                    label="Email Address"
                    placeholder="Email Address"
                    inputIcon={media.email}
                    onChangeText={(text) => onChange({name: 'email', value: text})}
                />

                <OutlinedInput
                    label="Password"
                    placeholder="Password"
                    inputIcon={media.padlock}
                    showEyeIcon={showEyeIcon}
                    onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                    onChangeText={(text) => onChange({name: 'password', value: text})}
                    isPassword={true}
                />
            </View>
            
            <View>
                <LoginButton title="LOG IN" onPress={() => {signUpFunc()}} />

                <View style={styles.alreadyTextContainer} >
                    <Text style={styles.alreadyText1} >DON'T HAVE AN ACCOUNT? </Text>
                    <TouchableOpacity onPress={() => {navigation.navigate('SignUp')}} >
                        <Text style={styles.alreadyText2} > SIGN UP</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white
    },
    alreadyText1: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    alreadyText2: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
        textDecorationLine: 'underline',
    },
    alreadyTextContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 12
    },
})

export default LoginScreen
import React, { useRef, useState, useEffect } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, Modal, Pressable, SafeAreaView, ScrollView, } from 'react-native';

import { colors } from '../../../global/colors';
import { screenWidth } from '../../../global/constants';
import { fontSize } from '../../../global/fontFamily';
import { media } from '../../../global/media';

import { useDispatch, useSelector } from 'react-redux';
import LoginButton from '../../../components/button/LoginButton';
import { emptyCartList } from '../../../redux/home/HomeActions';


const ConfirmOrder = ({address, paymentMode, navigation}) => {
    
    const dispatch = useDispatch();

    const [subTotal, setSubTotal] = useState(0);
    const [shipppingFee, setShipppingFee] = useState(99);

    const cart_list = useSelector(state => state.HomeReducer.cart_list);
   
    

    useEffect(() => {
        calculateTotalPrice()
    
    }, [])
    

    const calculateTotalPrice = () => {
        let sum = 0;
      
        cart_list.forEach(item => {
          sum += item.price;
        });

        setSubTotal(sum)
        
    }


    const OrderItems = () => {
        return(
            <View style={{width: screenWidth-40, marginBottom: 20}} >
                <View style={{backgroundColor: colors.light_gray,  alignItems: 'flex-start',  padding: 20, paddingTop: 10, borderRadius: 10}} >
                    <Text style={styles.header} >Order Items ({cart_list.length} {cart_list.length == 1 ? 'item' : 'items'})</Text>

                    <View>
                        {cart_list.map((item, index) => (
                            <View key={index} style={[{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', marginVertical: 10,}, cart_list.length -1 != index ? { borderBottomWidth: 1, borderColor: colors.gray, paddingBottom: 20,} : null ]} >
                                <Image source={{uri: item.image_url}} style={{height: 50, width: 50, borderRadius: 10}} />
        
                                <View style={{flex: 1, marginHorizontal: 15, }} >
                                    <Text style={styles.title} >{item.item_name}</Text>
                                    <Text style={styles.subtitle}  numberOfLines={2} >{item.item_description}</Text>
                                </View>
        
                                <Text style={styles.subTitle} >Rs. {item.price}</Text>
                            </View>
                        ))}
                    </View>
                </View>
            </View>
        )
    }


    const OrderInfo = () => {
        return(
            <View style={{width: screenWidth-40, marginBottom: 20}} >
                <View style={{backgroundColor: colors.light_gray,  alignItems: 'flex-start',  padding: 20, paddingTop: 10, borderRadius: 10}} >
                    <Text style={styles.header} >Order Info</Text>

                    <View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'space-between', width: '100%',}} >
                        <Text style={styles.subTitle} >Subtotal</Text>
                        <Text style={styles.subTitle} >Rs. {subTotal}</Text>
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'space-between', width: '100%',}} >
                        <Text style={styles.subTitle} >Shipping Fee</Text>
                        <Text style={styles.subTitle} >Rs. {shipppingFee}</Text>
                    </View>

                    <View style={styles.dashLine} />

                    <View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'space-between', width: '100%',}} >
                        <Text style={styles.subTitle} >Order Total</Text>
                        <Text style={styles.subTitle} >Rs. {subTotal+shipppingFee}</Text>
                    </View>
                </View>
            </View>
        )
    }

    const ShippingDetails = () => {
        return(
            <View style={{width: screenWidth-40, marginBottom: 20}} >
                <View style={{backgroundColor: colors.light_gray,  alignItems: 'flex-start',  padding: 20, paddingTop: 10, borderRadius: 10}} >
                    <Text style={styles.header} >Shipping Details</Text>

                    <View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'space-between', width: '100%',}} >
                        <Text style={styles.subTitle} >Recipient</Text>
                        <Text style={styles.subTitle} >{address.fullName}</Text>
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'space-between', width: '100%',}} >
                        <Text style={styles.subTitle} >Mobile Number</Text>
                        <Text style={styles.subTitle} >{address.number}</Text>
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'space-between', width: '100%',}} >
                        <Text style={styles.subTitle} >Address</Text>
                        <View style={{width: 180, alignItems: 'flex-end',}} >
                            <Text style={styles.subTitle} numberOfLines={2} >{address.address}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    const PaymentMethod = () => {
        return(
            <View style={{width: screenWidth-40, marginBottom: 20}} >
                <View style={{backgroundColor: colors.light_gray,  alignItems: 'flex-start',  padding: 20, paddingTop: 10, borderRadius: 10}} >
                    <Text style={styles.header} >Payment Method</Text>

                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%',}} >
                        <Text style={styles.subTitle} >Cash on Delivery</Text>
                    </View>

                </View>
            </View>
        )
    }
    

    const emptyCartListFunc = () => {
        dispatch(emptyCartList())
    }
    return (
        <View style={styles.container} >
             <Text style={styles.header} >Confirm Order</Text>
            <ScrollView style={{width: '100%', }} contentContainerStyle={{alignItems: 'center'}} >
               
                

                <OrderItems />
                <OrderInfo />
                <ShippingDetails /> 
                <PaymentMethod /> 

            </ScrollView>

            <View>
                <LoginButton 
                    title="CONFIRM ORDER"
                    onPress={() => {
                        emptyCartListFunc()
                        navigation.navigate('Cart'); 
                        navigation.navigate('OrderConfirmation');
                    }} 
                />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent:'space-between',
        width: screenWidth,
        backgroundColor: colors.white,
        //padding: 20,
    },
    title: {
        fontSize: fontSize.SubTitle,
        color: colors.black,
        fontWeight: '700',
        marginBottom: 4,
    },
    subtitle: {
        fontSize: fontSize.Body,
        color: colors.black,
        fontWeight: '500',
    },
    subTitle: {
        fontSize: fontSize.SubTitle,
        color: colors.black,
        fontWeight: '500',
    },

    header: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 10,
        textAlign: 'center'
    },
    dashLine: {
        borderStyle: 'dashed',
        borderWidth: 1,
        borderRadius: 1,
        height: 1,
        width: '100%',
        marginTop: 10,
        borderColor: colors.gray
    }
});




export default ConfirmOrder
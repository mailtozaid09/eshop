import React, { useRef, useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, Modal, Pressable, SafeAreaView, ScrollView, } from 'react-native';

import { colors } from '../../../global/colors';
import { screenWidth } from '../../../global/constants';
import { fontSize } from '../../../global/fontFamily';
import { media } from '../../../global/media';

import { useDispatch, useSelector } from 'react-redux';
import LoginButton from '../../../components/button/LoginButton';


const Payment = ({increaseStepCount, getPaymentMode}) => {

    const [chosenOption, setChosenOption] = useState('cashOnDelivery'); 
    const options = [
        { label: 'Cash on Delivery', value: 'cashOnDelivery' },
        { label: 'Pay with Debit/Credit/ATM Cards', value: 'card' },
        { label: 'Net Banking', value: 'netBanking' },
        { label: 'Other UPI Apps', value: 'otherUPIApps' },
        { label: 'EMI', value: 'emi' },
    ];
    
    return (
        <View style={styles.container} >
            <View style={{width: '100%'}} >
                <Text style={styles.header} >Select a payment method</Text>

                <View style={{width: '100%'}} >
                    {options.map((item) => (
                        <TouchableOpacity 
                            onPress={() => setChosenOption(item.value)}
                            style={{height: 50, justifyContent: 'center', paddingLeft: 20,  backgroundColor: colors.light_gray, borderWidth: 1, borderColor: chosenOption == item.value ? colors.dark_blue : colors.gray, borderRadius: 10, marginBottom: 15,}} >
                            <Text style={styles.title} >{item.label}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            </View>

            <LoginButton 
                   
                    title="CONTINUE"
                    onPress={() => {
                        increaseStepCount();
                        getPaymentMode(chosenOption)
                    }} 
                />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent:'space-between',
        width: screenWidth,
        backgroundColor: colors.white,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    header: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 10,
        textAlign: 'center'
    },
});




export default Payment
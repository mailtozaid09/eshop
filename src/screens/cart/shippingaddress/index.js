import React, { useRef, useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, Modal, Pressable, SafeAreaView, ScrollView, } from 'react-native';

import { colors } from '../../../global/colors';
import { screenWidth } from '../../../global/constants';
import { fontSize } from '../../../global/fontFamily';
import { media } from '../../../global/media';

import { useDispatch, useSelector } from 'react-redux';
import LoginButton from '../../../components/button/LoginButton';
import ShippingAddressSheet from '../../../components/modal/ShippingAddressSheet';

const ShippingAddress = ({increaseStepCount, getShippingAddress}) => {

    const [form, setForm] = useState({});
    const [address, setAddress] = useState('');

    const refRBSheet = useRef();

    const saved_address = useSelector(state => state.HomeReducer.saved_address);


    const EmptyList = ({openRBSHEET}) => {
        return(
            <View style={{flex: 1, backgroundColor: 'red', alignItems: 'center', justifyContent: 'space-between'}} >
                
                
                <Image  source={media.app_logo}  style={{height: 100 ,width: 100}} />

                
                
                <LoginButton 
                    title="ADD SHIPPING ADDRESS"
                    onPress={openRBSHEET} 
                />

                
            </View>
        )
    }


    const saveAddressDetails = (addressDetails) => {

        setAddress(addressDetails.address)

        var newAdd = addressDetails.address + ", " +  addressDetails.city + ", " +  addressDetails.region + ", " +  addressDetails.country + ", " +  addressDetails.zipcode 
       

        var value = {
            "address": newAdd, 
            "number": addressDetails.number, 
            "city": addressDetails.city, 
            "country": addressDetails.country, 
            "fullName": addressDetails.fullName, 
            "id": addressDetails.id, 
            "region": addressDetails.region, 
            "zipcode": addressDetails.zipcode, 
        }

        setForm({ ...form, ['address']: value });
    };


    return (
        <View style={styles.container}>

            {saved_address.length == 0
            ? 
                <EmptyList   openRBSHEET={() => {refRBSheet.current.open()}} />
            : 
                <View style={{flex: 1, justifyContent: 'space-between'}} >
                    <ScrollView>
                    <Text style={styles.header} >Select Shipping Address</Text>
                    {saved_address.map((item) => (
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            onPress={() => {saveAddressDetails(item);}} 
                            style={{backgroundColor: address == item.address ? colors.light_gray : colors.white, width: screenWidth-40, borderWidth: 2, borderColor: address == item.address ? colors.dark_blue : colors.gray, borderRadius: 10, padding: 15, marginBottom: 15}} >
                            <Text style={styles.fullName} >{item.fullName}</Text>
                            <Text style={styles.address} >{item.address}</Text>
                            <Text style={styles.city} >{item.city}, {item.region}, {item.zipcode}</Text>
                            <Text>Use as the shipping address</Text>
                        </TouchableOpacity>
                    ))}
                    </ScrollView>


                    <View style={{ alignItems: 'flex-end'}} >
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            onPress={() => {refRBSheet.current.open()}} 
                            style={{height: 50, width: 50, alignItems: 'center', justifyContent: 'center', borderRadius: 25, backgroundColor: colors.dark_blue}} >
                            <Image source={media.plus} style={{height: 34, width: 34}} />
                        </TouchableOpacity>
                    </View>

                <LoginButton 
                    disabled={!address ? true : false}
                    title="CONTINUE"
                    onPress={() => {
                        increaseStepCount();
                        getShippingAddress(form);
                    }} 
                />
                </View>
            }

                <ShippingAddressSheet refRBSheet={refRBSheet} closeRBSHEET={() => {refRBSheet.current.close()}}  />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        width: screenWidth,
        backgroundColor: colors.white,
        //padding: 20,
    },
    fullName: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
     
    },
    address: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 10,
    },
    city: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
        marginBottom: 10
    },
    header: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 10,
        textAlign: 'center'
    },
});



export default ShippingAddress
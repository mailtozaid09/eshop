import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, Modal, Pressable, SafeAreaView, ScrollView, } from 'react-native';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';

import { useDispatch, useSelector } from 'react-redux';

import CheckoutSteps from './CheckoutSteps';
import ShippingAddress from './shippingaddress';
import Payment from './payment';
import ConfirmOrder from './confirmorder';



const Checkout = ({navigation}) => {

    const dispatch = useDispatch();

    const [address, setAddress] = useState({});
    const [paymentMode, setPaymentMode] = useState({});
  

    const [activeStep, setActiveStep] = useState('payment');
    const [activeStepCount, setActiveStepCount] = useState(0);

    const promo_code = useSelector(state => state.HomeReducer.promo_code);

    const getShippingAddress = (address) => {
        setAddress(address.address)
    }

    const getPaymentMode = (payment) => {
        setPaymentMode(payment)
    }

    return (
        <SafeAreaView style={styles.container}>
            <CheckoutSteps activeStep={activeStep} activeStepCount={activeStepCount} />

            {activeStepCount == 0 
            ?
                <ShippingAddress getShippingAddress={(address) => getShippingAddress(address)} increaseStepCount={() => setActiveStepCount(prev => prev + 1)} />
            : 
            activeStepCount == 1 ?
                <Payment getPaymentMode={(payment) => getPaymentMode(payment)} increaseStepCount={() => setActiveStepCount(prev => prev + 1)} />
            :
                <ConfirmOrder address={address} paymentMode={paymentMode} navigation={navigation} increaseStepCount={() => setActiveStepCount(prev => prev + 1)}/>
            }
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white,
        padding: 20,
    },
    title: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 10,
    },
});



export default Checkout
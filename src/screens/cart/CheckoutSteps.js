import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, FlatList, Modal, Pressable, SafeAreaView, ScrollView, } from 'react-native';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';

import { useDispatch, useSelector } from 'react-redux';


const CheckoutSteps = ({activeStep, activeStepCount}) => {
    return (
        <View style={styles.container} >
            <Image source={activeStepCount >= 0 ? media.location : media.location_outlined} style={{height: 28, width: 28}} />
            
            <View style={[styles.dashLine, {borderColor: activeStepCount > 0 ? colors.dark_blue : colors.light_gray}]} />
            
            <Image source={activeStepCount >= 1 ? media.wallet : media.wallet_outlined} style={{height: 24, width: 24}} />

            <View style={[styles.dashLine, {borderColor: activeStepCount > 1 ? colors.dark_blue  : colors.light_gray}]} />
            
            <Image source={activeStepCount >= 2 ? media.booking : media.booking_outlined} style={{height: 28, width: 28}} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    dashLine: {
        borderStyle: 'dashed',
        borderWidth: 1,
        borderRadius: 1,
        height: 1,
        width: 100,
        borderColor: 'red'
    }
})
export default CheckoutSteps
import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { useDispatch, useSelector } from 'react-redux'
import { fontSize } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'
import { removeItemFromCart, setPromoCodeValue } from '../../redux/home/HomeActions'
import LoginButton from '../../components/button/LoginButton'
import PromoCodes from '../../components/modal/PromoCodes'
  


const CartScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const [showOfferModal, setShowOfferModal] = useState(false);

    const cart_list = useSelector(state => state.HomeReducer.cart_list);

    const promo_code = useSelector(state => state.HomeReducer.promo_code);

    const removeItemFromCartFunc = (item) => {
        dispatch(removeItemFromCart(item.item_name))
    }

    const calculateTotalPrice = () => {
        let sum = 0;
      
        cart_list.forEach(item => {
          sum += item.price;
        });
      
        console.log(sum);
        return sum;
    }


    const CartItems = () => {
        return(
            <View style={{width: screenWidth, alignItems: 'center', flex: 1}} >
                 <ScrollView>
                    <View style={{padding: 20, marginBottom: 10, width: screenWidth-10,}} >
                        {cart_list?.map((item) => (
                            <View style={{backgroundColor: colors.light_gray, marginBottom: 20, padding: 20, borderRadius: 10, flexDirection: 'row', alignItems: 'center'}} >
                                <Image source={{uri: item.image_url}} style={{height: 80, width: 80, borderRadius: 10, marginRight: 15}} />
                                <View style={{flex: 1}} >
                                    <Text numberOfLines={1} style={styles.title} >{item.item_name}</Text>
                                    <Text numberOfLines={2} style={styles.description} >{item.item_description}</Text>
                                    <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 8}} >
                                        <Image source={media.star} style={{height: 12,width: 12, marginRight: 6}} />
                                        <Text style={styles.ratings} >{item.ratings} Ratings</Text>
                                    </View>
                                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                        <Text style={styles.price} >Rs. {item.price}</Text>
                                        <TouchableOpacity
                                            onPress={() => {removeItemFromCartFunc(item)}}
                                        >
                                            <Image source={media.delete} style={{height: 28,width: 28, marginRight: 6}} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                </View>
                            </View>
                        )) }
                    </View>
                    </ScrollView>

            <View style={{ marginTop: 10, marginBottom: 10}} >
                

                {promo_code == '' ?
                <TouchableOpacity 
                    onPress={() => setShowOfferModal(true)}
                    style={{ height: 65, backgroundColor: colors.white, borderWidth: 2, borderColor: colors.light_gray, borderRadius: 10, paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                    
                    <View style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Text style={{fontSize: fontSize.Body, fontWeight: '600', color: colors.dark_blue}} >Apply Promo Codes</Text>
                        <Text style={{fontSize: fontSize.Body, fontWeight: '600', color: colors.gray}} > (Save upto 50%)</Text>
                    </View>
                    <View style={{height: 50, width: 50, borderRadius: 25, marginLeft: 20, backgroundColor: colors.light_gray, alignItems: 'center', justifyContent: 'center'}} >
                        <Image source={media.right_arrow} style={{height: 28,width: 28,}} />
                    </View>
                </TouchableOpacity>
                :
                <TouchableOpacity 
                    onPress={() => setShowOfferModal(true)}
                    style={{ height: 60, backgroundColor: colors.light_gray, borderRadius: 10, paddingHorizontal: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                    
                    <View style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Text style={{fontSize: fontSize.Title, fontWeight: '600', color: colors.dark_blue}} >{promo_code}</Text>
                        <Text style={{fontSize: fontSize.Body, fontWeight: '600', color: colors.gray}} > (Save upto 50%)</Text>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {dispatch(setPromoCodeValue(''))}}
                        >
                        <Image source={media.close} style={{height: 20,width: 20,}} />
                    </TouchableOpacity>
                </TouchableOpacity>
                }

                <View style={{flexDirection: 'row', marginVertical: 0, alignItems: 'center', justifyContent: 'space-between'}} >
                    <Text style={styles.totalPrice} >Total Price:  </Text>
                    <Text style={styles.totalPrice} >Rs. {calculateTotalPrice()}</Text>
                </View>
                <LoginButton
                    title="Checkout"
                    onPress={() => {
                        navigation.navigate('Checkout');
                    }}
                />
            </View>
            </View>
        )
    }

    const EmptyList = () => {
        return(
            <View style={{paddingHorizontal: 30, marginBottom: 40, flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                <Image source={media.empty_cart} style={{height: 240, width: 240}} />
                <Text style={styles.empty_title} >Your Cart is Empty</Text>
                <Text style={styles.empty_subtitle} >Looks like you haven't added anything to your cart</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            {cart_list.length != 0 
            ?   
                <CartItems />
            :
                <EmptyList />    
            }

            {showOfferModal && (
                <PromoCodes showPromoModal={showOfferModal} closeModalFunc={() => setShowOfferModal(false)} />
            )}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white,
    },
    empty_title: {
        fontSize: fontSize.SubHeading,
        color: colors.dark_blue,
        fontWeight: '600',
        marginTop: 15,
    },
    empty_subtitle: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 10,
    },
    title: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 10,
    },
    description: {
        fontSize: fontSize.Body,
        color: colors.dark_blue,
        fontWeight: '700',
        marginBottom: 10
    },
    ratings: {
        fontSize: 16,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    price: {
        fontSize: 18,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    totalPrice: {
        fontSize: 18,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 20,
    }
})

export default CartScreen
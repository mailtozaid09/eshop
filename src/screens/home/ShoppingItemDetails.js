import React, { useState, useEffect } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, TextInput, Image, FlatList, SafeAreaView, ScrollView, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';
import { useDispatch, useSelector } from 'react-redux';

import ShoppingList from '../../components/custom/home/ShoppingList';
import { shoppingListData } from '../../global/sampleData';
import { useNavigation } from '@react-navigation/native';
import { addItemToCart, addItemToWishlist, removeItemFromCart, removeItemFromWishlist } from '../../redux/home/HomeActions';



const ShoppingItemDetails = (props) => {
    const dispatch = useDispatch();

    const navigation = useNavigation();

    const [details, setDetails] = useState(props.route.params.params);

    const wish_list = useSelector(state => state.HomeReducer.wish_list);
    const cart_list = useSelector(state => state.HomeReducer.cart_list);

    const [shoppingArr, setshoppingArr] = useState(shoppingListData);
    const [originalArr, setOriginalArr] = useState(shoppingListData);

    useEffect(() => {
        changeCategoryFilter(details.category)
    }, [])

    const changeCategoryFilter = (category) => {

        if(category != 'All'){
            var filteredList = originalArr.filter((item) => {
                return item.category == category;
            });

            setshoppingArr(filteredList)

        }else{
            setshoppingArr(originalArr)
        }
    }

    
    const ItemDetails = () => {
        return(
            <View>
                
                <View>
                    <Image source={{uri: details.image_url}} style={{height: 300, resizeMode: 'contain'}} />
                
                </View>
                <View style={{width: screenWidth, padding: 20}} >
                    <Text numberOfLines={1} style={styles.title} >{details.item_name}</Text>
                    
                    <Text numberOfLines={3} style={styles.description} >{details.item_description}</Text>

                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                        <View style={{}} >
                            <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 8}} >
                                
                                <Text style={styles.ratings} >{details.ratings}</Text>
                                <Image source={media.star} style={{height: 24,width: 24, marginHorizontal: 6}} />
                                <Text style={styles.ratings} > Ratings</Text>
                            </View>
                        </View>
                        <View style={{}} >
                            <Text style={styles.price} >Rs. {details.price}</Text>
                        </View>
                        </View>
                </View>
            </View>
        )
    }


   


    const dishIncludedInCartList = (item_name) => {
        return cart_list?.some(value => value['item_name'] == item_name);
    }
    


   const addItemToCartFunc = (item) => {
    

    if(dishIncludedInCartList(item.item_name)){
        console.log("removce");
        dispatch(removeItemFromCart(item.item_name))
    }else{
        console.log("adddd");
        var favItem = {
            image_url: item.image_url,
            category: item.category,
            item_name: item.item_name,
            item_description: item.item_description,
            price: item.price,
            ratings: item.ratings,
        }
        dispatch(addItemToCart(favItem))
    }
}

const itemIncludedInWishlist = (item_name) => {
    return wish_list?.some(value => value['item_name'] == item_name);
}

const addItemToWishlistFunc = (item) => {

    if(itemIncludedInWishlist(item.item_name)){
        dispatch(removeItemFromWishlist(item.item_name))
    }else{
        var favItem = {
            image_url: item.image_url,
            category: item.category,
            item_name: item.item_name,
            item_description: item.item_description,
            price: item.price,
            ratings: item.ratings,
        }
        dispatch(addItemToWishlist(favItem))
    }
}


    return (
        <SafeAreaView style={styles.container} >
           

            <ScrollView>
            <ItemDetails />

           

            <Text style={styles.recommended} >Recommended for you</Text>
            <ShoppingList
                isHorizontal={true}
                shoppingData={shoppingArr} 
            />


            </ScrollView>
        

            <View style={{width: screenWidth, flexDirection: 'row', paddingHorizontal: 20, justifyContent: 'space-between', alignItems: 'center', }} >
                <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => addItemToWishlistFunc(details)}
                style={styles.circularButtonContainer}
            >
                    <Image source={itemIncludedInWishlist(details.item_name)  ? media.heart_fill : media.heart_outline} style={{height: 34, width: 34}} />  
                </TouchableOpacity>



                {dishIncludedInCartList(details.item_name) 
                    ? 
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => addItemToCartFunc(details)}
                        style={[styles.buttonContainer, {backgroundColor: colors.white, borderColor: colors.dark_blue, borderWidth: 1,}]} >
                        <Text style={{fontSize: fontSize.SubTitle, color: colors.dark_blue, fontWeight: '600'}} >REMOVE FROM CART</Text>
                    </TouchableOpacity>
                        
                    :
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => addItemToCartFunc(details)}
                        style={[styles.buttonContainer, {backgroundColor: colors.dark_blue,}]} >
                        <Text style={{fontSize: fontSize.SubTitle, color: colors.white, fontWeight: '600'}} >ADD TO CART</Text>
                    </TouchableOpacity>
                }
                
                
            </View>
        </SafeAreaView>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white,
    },
    title: {
        fontSize: fontSize.Heading,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 10,
    },
    recommended: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginHorizontal: 20,
        marginBottom: 15,
    },
    description: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '500',
        marginVertical: 10
    },
    ratings: {
        fontSize: 20,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    price: {
        fontSize: 20,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    totalPrice: {
        fontSize: 18,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 20,
    },
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 60,
        borderRadius: 30,
        marginVertical: 10,
        marginLeft: 10,
    },
    circularButtonContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.light_gray,
    },
    buttonText: {
        fontSize: fontSize.SubTitle,
        fontWeight: 'bold',
        textAlign: 'center',
        color: colors.white,
    },
})

export default ShoppingItemDetails
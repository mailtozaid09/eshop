import React, { useState, useEffect } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, TextInput, Image, FlatList, SafeAreaView, ScrollView, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';
import { useDispatch, useSelector } from 'react-redux';

import ShoppingList from '../../components/custom/home/ShoppingList';
import { shoppingListData, suggestions_data } from '../../global/sampleData';
import { useNavigation } from '@react-navigation/native';
import { addItemToCart, addItemToWishlist, removeItemFromCart, removeItemFromWishlist } from '../../redux/home/HomeActions';
import Input from '../../components/input';



const SearchScreen = (props) => {
    const dispatch = useDispatch();

    const navigation = useNavigation();

    const wish_list = useSelector(state => state.HomeReducer.wish_list);
    const cart_list = useSelector(state => state.HomeReducer.cart_list);

    const [searchValue, setSearchValue] = useState('');

    const [originalArray, setOriginalArray] = useState(shoppingListData);
    const [shoppingArr, setShoppingArr] = useState(shoppingListData);


    useEffect(() => {
        //changeCategoryFilter(details.category)
    }, [])

    const changeCategoryFilter = (category) => {

        if(category != 'All'){
            var filteredList = originalArr.filter((item) => {
                return item.category == category;
            });

            setshoppingArr(filteredList)

        }else{
            setshoppingArr(originalArr)
        }
    }


    const searchFilterFunction = (text) => {
        var shoppingList = shoppingListData
        if (text) {
            const newData = shoppingList.filter(
                function (item) {
                const itemData = item.item_name
                    ? item.item_name.toUpperCase()
                    : ''.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
        });

          
            setShoppingArr(newData);
            setSearchValue(text);
        } else {
            setShoppingArr(shoppingList);
            setSearchValue(text);
        }
    };


    const Suggestions = () => {
        return(
            <View style={{paddingHorizontal: 20}} >
                <Text style={styles.suggestionsTitle} >Suggestions</Text>
                <View style={styles.suggestionsContainer} >
                    {suggestions_data.map((item) => (
                        <TouchableOpacity 
                            onPress={() => {searchFilterFunction(item.title);}}
                            style={styles.suggestionsItem} >
                            <Text style={styles.suggestionsItemText} >{item.title}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
        )
    }


    const SearchResult = () => {
        return(
            <View>
                  <View style={{padding: 20, marginBottom: 10, width: '100%'}} >
                        {shoppingArr?.map((item) => (
                            <TouchableOpacity 
                                activeOpacity={0.5}
                                onPress={() => {navigation.navigate('ShoppingItemDetails', {params: item})}}
                                style={{backgroundColor: colors.light_gray, marginBottom: 20, padding: 20, borderRadius: 10, flexDirection: 'row', alignItems: 'center'}} >
                                <Image source={{uri: item.image_url}} style={{height: 80, width: 80, borderRadius: 10, marginRight: 15}} />
                                <View style={{flex: 1}} >
                                    <Text numberOfLines={1} style={styles.title} >{item.item_name}</Text>
                                    <Text numberOfLines={2} style={styles.description} >{item.item_description}</Text>
                                    
                                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                            
                                        <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 8, marginTop: 10}} >
                                            <Image source={media.star} style={{height: 12,width: 12, marginRight: 6}} />
                                            <Text style={styles.ratings} >{item.ratings}</Text>
                                        </View>
                                        <Text style={styles.price} >Rs. {item.price}</Text>
                                        
                                    </View>
                                    
                                </View>
                            </TouchableOpacity>
                        )) }
                    </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={{width: screenWidth, alignItems: 'center', flex: 1}} >

                <ScrollView contentContainerStyle={{width: screenWidth, }} >
                    <View style={{paddingBottom: 0, padding: 20}} >
                        <Input
                            isSearch={true}
                            placeholder="Search"
                            value={searchValue}
                            onClearSearchText={() => {setSearchValue(''); setShoppingArr(originalArray);}}
                            onChangeText={(text) =>  {searchFilterFunction(text)}}
                        />
                    </View>

                    {!searchValue ? 
                    <Suggestions />
                        :
                    <SearchResult />
                    }

                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 20,
        backgroundColor: colors.white
    },
    title: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    fullname: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 20,
    },
    suggestionsContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        width: screenWidth-40,
        marginTop: 15,
    },
    suggestionsTitle: {
        fontSize: fontSize.SubHeading,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    suggestionsItem: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        paddingHorizontal: 20,
        margin: 5,
        borderRadius: 10,
        backgroundColor: colors.dark_blue
    },
    suggestionsItemText: {
        fontSize: fontSize.SubTitle,
        fontWeight: '500',
        color: colors.white,
    },

})
export default SearchScreen
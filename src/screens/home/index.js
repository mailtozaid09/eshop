import React, {useEffect, useState} from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image } from 'react-native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'

import CategoriesList from '../../components/custom/home/CategoriesList'
import ShoppingList from '../../components/custom/home/ShoppingList'
  
import { shoppingListData } from '../../global/sampleData';
import { useSelector } from 'react-redux'
import BannerCarousel from '../../components/carousel/BannerCarousel'

const HomeScreen = ({navigation}) => {


    const cart_list = useSelector(state => state.HomeReducer.cart_list);
    
    const [shoppingArr, setshoppingArr] = useState(shoppingListData);
    const [originalArr, setOriginalArr] = useState(shoppingListData);



    useEffect(() => {
        shuffleData()
    }, [])


    
    const shuffleData = () => {
        let unshuffled = shoppingListData


        let shuffled = unshuffled
            .map(value => ({ value, sort: Math.random() }))
            .sort((a, b) => a.sort - b.sort)
            .map(({ value }) => value)
        
        setOriginalArr(shuffled)
        setshoppingArr(shuffled)
    }



    const changeCategoryFilter = (category) => {

        if(category != 'All'){
            var filteredList = originalArr.filter((item) => {
                return item.category == category;
            });

            setshoppingArr(filteredList)

        }else{
            setshoppingArr(originalArr)
        }
    }


    return (
        <SafeAreaView style={styles.container} >
            

           <ScrollView>
                <CategoriesList 
                    changeCategoryFilter={(category) => changeCategoryFilter(category)} 
                />


                {/* <BannerCarousel /> */}
      

                <View style={{marginBottom: 0}} >
                    <ShoppingList 
                        shoppingData={shoppingArr} 
                    />
                </View>
           </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white
    }
})

export default HomeScreen
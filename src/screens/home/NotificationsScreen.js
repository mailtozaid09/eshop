import React, { useState, useEffect } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, TextInput, Image, FlatList, SafeAreaView, ScrollView, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';
import { useDispatch, useSelector } from 'react-redux';

import { useNavigation } from '@react-navigation/native';
import { addItemToCart, addItemToWishlist, removeItemFromCart, removeItemFromWishlist } from '../../redux/home/HomeActions';



const NotificationsScreen = (props) => {
    const dispatch = useDispatch();

    const navigation = useNavigation();


    const wish_list = useSelector(state => state.HomeReducer.wish_list);

    useEffect(() => {
      
    }, [])

    const EmptyList = () => {
        return(
            <View style={{paddingHorizontal: 30, marginBottom: 40, flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                <Image source={media.empty_notification} style={{height: 240, width: 240}} />
                <Text style={styles.empty_title} >No notifications yet</Text>
                <Text style={styles.empty_subtitle} >Come back here to get information about exciting offers, your order details and much more!</Text>
            </View>
        )
    }

    const NotificationList = () => {
        return(
            <View>
                <Text>NotificationsScreen</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            {wish_list.length != 0 
            ?   
                <NotificationList />
            :
                <EmptyList />    
            }
            

            

        </SafeAreaView>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white,
    },
    empty_title: {
        fontSize: fontSize.SubHeading,
        color: colors.dark_blue,
        fontWeight: '600',
        marginTop: 15,
    },
    empty_subtitle: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 10,
    },
    title: {
        fontSize: fontSize.Heading,
        color: colors.dark_blue,
        fontWeight: '700',
        marginTop: 10,
    },
})

export default NotificationsScreen
import React, { useState, useEffect } from 'react'
import { Text, View, ScrollView, SafeAreaView, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import OutlinedInput from '../../components/input/OutlinedInput'
import LoginButton from '../../components/button/LoginButton'
  


const EditProfile = ({navigation}) => {

    const [showEyeIcon, setShowEyeIcon] = useState(false);

    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


   
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const updateProfileDetails = () => {
        console.log("form => ", form);
        navigation.goBack()
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={{padding: 20, alignItems: 'center', }} >
                
                <View style={{marginBottom: 40, marginTop: 20}} >
                    <View style={{height: 100, width: 100, borderRadius: 50, alignItems: 'center', justifyContent: 'center', borderWidth: 1}} >
                        <Image source={media.user_blue} style={{height: 50, width: 50, marginLeft: 10}} />
                    </View>
                    
                    <TouchableOpacity 
                        activeOpacity={0.5}
                        onPress={() => {}}
                        style={{ position: 'absolute', bottom: 0, right: -5, height: 40, width: 40, backgroundColor: colors.light_gray, borderRadius: 20, alignItems: 'center', justifyContent: 'center'}} >
                        <Image source={media.edit_blue} style={{height: 20, width: 20,}} />
                    </TouchableOpacity>
                </View>
                
                <OutlinedInput
                    label="Name"
                    placeholder="Full Name"
                    inputIcon={media.user_blue}
                    onChangeText={(text) => onChange({name: 'name', value: text})}
                />


                <OutlinedInput
                    label="Email Address"
                    placeholder="Email Address"
                    inputIcon={media.email}
                    onChangeText={(text) => onChange({name: 'email', value: text})}
                />

            </View>

            <View style={{marginBottom: 10}}>
                <LoginButton
                    title="UPDATE PROFILE"
                    onPress={() => {updateProfileDetails()}}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 20,
        justifyContent: 'space-between',
        backgroundColor: colors.white
    },
    title: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    fullname: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 20,
    },
    profileContainer: {
        backgroundColor: colors.light_gray,
        borderRadius: 10,
        padding: 10,
        paddingLeft: 20,
        height: 60,
        marginBottom: 15,
        width: screenWidth-40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})

export default EditProfile
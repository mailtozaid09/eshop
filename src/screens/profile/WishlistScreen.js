import React from 'react'
import { Text, View, ScrollView, SafeAreaView, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { useDispatch, useSelector } from 'react-redux'
import { removeItemFromWishlist } from '../../redux/home/HomeActions'
  


const WishlistScreen = () => {

    const dispatch = useDispatch();
    
    const wish_list = useSelector(state => state.HomeReducer.wish_list);

    const removeItemFromWishlistFunc = (item) => {
        dispatch(removeItemFromWishlist(item.item_name))
    }

    const WishListItems = () => {
        return(
            <View style={{width: screenWidth, alignItems: 'center', flex: 1}} >
                 <ScrollView>
                    <View style={{padding: 20, marginBottom: 10, width: screenWidth-10,}} >
                        {wish_list?.map((item) => (
                            <View style={{backgroundColor: colors.light_gray, marginBottom: 20, padding: 20, borderRadius: 10, flexDirection: 'row', alignItems: 'center'}} >
                                <Image source={{uri: item.image_url}} style={{height: 80, width: 80, borderRadius: 10, marginRight: 15}} />
                                <View style={{flex: 1}} >
                                    <Text numberOfLines={1} style={styles.title} >{item.item_name}</Text>
                                    <Text numberOfLines={2} style={styles.description} >{item.item_description}</Text>
                                    <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 8}} >
                                        <Image source={media.star} style={{height: 12,width: 12, marginRight: 6}} />
                                        <Text style={styles.ratings} >{item.ratings}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                                        <Text style={styles.price} >Rs. {item.price}</Text>
                                        <TouchableOpacity
                                            onPress={() => {removeItemFromWishlistFunc(item)}}
                                        >
                                            <Image source={media.delete} style={{height: 28,width: 28, marginRight: 6}} />
                                        </TouchableOpacity>
                                    </View>
                                    
                                </View>
                            </View>
                        )) }
                    </View>
                    </ScrollView>
                </View>
        )
    }

    
    const EmptyList = () => {
        return(
            <View style={{paddingHorizontal: 30, marginBottom: 40, flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                <Image source={media.empty_wishlist} style={{height: 240, width: 240}} />
                <Text style={styles.empty_title} >Your Wishlist is Empty</Text>
                <Text style={styles.empty_subtitle} >Looks like you haven't added anything to your wishlist</Text>
            </View>
        )
    }


    return (
        <SafeAreaView style={styles.container} >
            {wish_list.length != 0 
            ?   
                <WishListItems />
            :
                <EmptyList />    
            }
             
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 20,
        backgroundColor: colors.white
    },
    empty_title: {
        fontSize: fontSize.SubHeading,
        color: colors.dark_blue,
        fontWeight: '600',
        marginTop: 15,
    },
    empty_subtitle: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '500',
        textAlign: 'center',
        marginTop: 10,
    },
    title: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    fullname: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 20,
    },

})

export default WishlistScreen
import React from 'react'
import { Text, View, ScrollView, SafeAreaView, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
  


const ProfileScreen = ({navigation}) => {

    const profileItems = [
        {
            id: 1,
            title: 'Notifications',
            icon: media.bell_blue,
            navigatePath: 'Notifications',
        },
        {
            id: 2,
            title: 'Wishlist',
            icon: media.bookmark_blue,
            navigatePath: 'Wishlist',
        },
        {
            id: 3,
            title: 'Shipping Address',
            icon: media.location_blue,
            navigatePath: 'ShippingAddress',
        },
        {
            id: 4,
            title: 'Promo Codes',
            icon: media.discount,
            navigatePath: 'PromoCodes',
        },
        {
            id: 5,
            title: 'Logout',
            icon: media.logout_blue,
            navigatePath: 'Logout',
        },
    ]

    return (
        <SafeAreaView style={styles.container} >
            <View style={{padding: 20, alignItems: 'center'}} >
                
                <View>
                    <View style={{height: 100, width: 100, borderRadius: 50, alignItems: 'center', justifyContent: 'center', borderWidth: 1}} >
                        <Image source={media.user_blue} style={{height: 50, width: 50, marginLeft: 10}} />
                    </View>
                    
                    <TouchableOpacity 
                        activeOpacity={0.5}
                        onPress={() => {navigation.navigate('EditProfile')}}
                        style={{ position: 'absolute', bottom: 0, right: -5, height: 40, width: 40, backgroundColor: colors.light_gray, borderRadius: 20, alignItems: 'center', justifyContent: 'center'}} >
                        <Image source={media.edit_blue} style={{height: 20, width: 20,}} />
                    </TouchableOpacity>
                </View>
                
                <Text style={styles.fullname} >Zaid Ahmed</Text>
                <Text style={styles.email} >mailtozaid09@gmail.com</Text>

                {profileItems.map((item) => (
                    <TouchableOpacity 
                        activeOpacity={0.5}
                        onPress={() => {navigation.navigate(item.navigatePath)}}
                        style={styles.profileContainer} >
                        <View style={{flexDirection: 'row', alignItems: 'center'}} >
                            <Image source={item.icon} style={{height: 28, width: 28, marginRight: 15}} />
                            <Text style={styles.title} >{item.title}</Text>
                        </View>

                        <Image source={media.right_arrow_small} style={{height: 22, width: 22, marginRight: 15}} />
                        
                    </TouchableOpacity>
                ))}
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 20,
        backgroundColor: colors.white
    },
    title: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
    },
    fullname: {
        fontSize: fontSize.Title,
        color: colors.dark_blue,
        fontWeight: '700',
        marginVertical: 20,
        marginBottom: 5
    },
    email: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '700',
        marginBottom: 20,
    },
    profileContainer: {
        backgroundColor: colors.light_gray,
        borderRadius: 10,
        padding: 10,
        paddingLeft: 20,
        height: 60,
        marginBottom: 15,
        width: screenWidth-40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
})

export default ProfileScreen